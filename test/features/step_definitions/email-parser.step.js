
module.exports = function () {

  'use strict';

  var assert = require('cucumber-assert'),
      World = require("../support/world.js").World,
      thisWorld = new World(),
      emailData,
      processedEmailData;

  this.Given('that a sender has written the following email containing several replies and multiple email signatures', function (emailTestData, done) {
      emailData = emailTestData;

      done();
  });

  this.When('the email parser processes the email thread', function (done) {
    processedEmailData = thisWorld.emailParseUtil.removeQuotedTextFromEmail(emailData);
    console.log("output", processedEmailData);
    done();
  });

  this.Then('the parser should only keep the last email:', function (expectedData, done) {
    assert.equal(processedEmailData, expectedData, done);
  });

  this.Given('that a sender has written the following short email', function (emailTestData, done) {
    emailData = emailTestData;

    done();
  });

  this.When('the email parser processes the short email', function (done) {
    processedEmailData = thisWorld.emailParseUtil.removeQuotedTextFromEmail(emailData);
    console.log("output", processedEmailData);
    done();
  });

  this.Then('the parser should provide the following result', function (expectedData, done) {
    assert.equal(processedEmailData, expectedData, done);
  });

};
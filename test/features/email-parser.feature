@only
Feature: Email Parser

Sleep On It should ignore email signatures when doing analysis

Scenario: 
Given that a sender has written the following email containing several replies and multiple email signatures
"""
Hi Cherie,
 
Access to our website is below.
 



 
 
Thank you.
 
Regards,
 
cid:image005.png@01CF6D4C.C0ADC730
Be GREEN! Please read from the screen.
This message is intended for the use of the party to whom it is addressed and may contain information that is confidential.  If you have received this mail in error you must not distribute, copy or take any action with respect to it, and notify Remedial Technologies Australia immediately.  Except for legitimate company matters, Remedial Technologies Australia Pty Ltd does not accept any responsibility for the opinions expressed in this email.  Attachments to this email may not have been checked for software viruses.
 
From: techprojectsau@zohosupport.com [mailto:techprojectsau@zohosupport.com] On Behalf Of Search Results Support
Sent: Monday, 11 January 2016 5:24 PM
To: RemTech Accounts <accounts@remtech.com.au>
Subject: Re: [##54329##] FTP Access
 
Hi Nour, 
 
Your website is the center of your online presence, it’s where google goes to cross-reference information that is broadcasted across the internet. One of the most crucial steps is making sure that your website is congruent with what all the work we are doing to get your Google Places listing on the front page. To do this we will need access to the following components of your website which you can get from your web developers or current website host.
 
FTP Access - This is the control panel for your hosting account for your website which will have a unique username and password. This will also be available from your website host.
 
Domain: 
 
Username: 
 
Password: 
 
Having access to the above components of your website are requirements for your campaigns ability to perform at its best. We will not be changing the way your website looks, just the back end meta tags that are used by search engines to determine what your business does.  We require this access within the next 7 days, the longer it takes for you to provide us with this information the longer it will take to get your business ranking on the front page. Please contact your website host to get this information. If you need further assistance please contact the support team at support@searchresults.com.au or give us a call at 1300 884 998.



We want to hear from you. How did you rate our customer service?
Good
Okay
Bad
 
 

Kind Regards,
 
Cherie
Customer Support Team
 
1300 884 998
http://www.searchresults.com.au/




 

 

Copyright © 2015 Search Results Specialists Pty Ltd. All Rights Reserved. 

 

The information contained in this e-mail is private and confidential and only intended to the recipient of this email.  Search Results have, in preparing this information used our best endeavours to ensure that the information contained therein is true and accurate, but accept no responsibility and disclaim all liability in respect of any errors, inaccuracies or misstatements contained herein. All parties interested in any Search Results product and /or service should make their own inquiries to verify the information contained herein. If you are not the intended recipient, you may not disclose or use the information in this e-mail in any way.  Search Results does not guarantee the integrity of any e-mails or attached files. The views or opinions expressed are the author's own and may not reflect the views or opinions of Search Results Specialists Pty Ltd. Search Results Specialists Pty Ltd is an independent service company that has no association or affiliation with Google. Google, AdWords and Google Places are registered or unregistered trademarks, and are the property of, Google Inc. or related entity.
"""
When the email parser processes the email thread
Then the parser should only keep the last email:
"""
Hi Cherie,
 
Access to our website is below.
"""

  Given that a sender has written the following short email
  """
  Hi Simone,
hope you're having a nice weekend:) Thanks for that info + questions. I've written answers below.

*We have a database of customers which buy specific brands across what we represent, some buy all brands, some buy less.
How many customers do you have in total?
*Can we sort by brand using your system?
Yes
*Can we record relationship details?  eg sales calls, appointments, emails etc...
Yes
*Can we use our ordering system Nuorder in conjunction with Zoho?
I'm not sure. Have contacted NuOrder for details about the API
*Does it talk to Mailchimp, so we can send out newsletters to specific buyers by brand?
Yes. You can also use Zoho Campaigns which does the same thing and has better integration.

best regards,

Jeremy Nagel
Lead Developer
Nuanced IT

E: jeremy.nagel@nuanced.it
M: (+61) 0414 885 787
Skype: jeznag
  """
  When the email parser processes the short email
  Then the parser should provide the following result
  """
  Hi Simone,
hope you're having a nice weekend:) Thanks for that info + questions. I've written answers below.

*We have a database of customers which buy specific brands across what we represent, some buy all brands, some buy less.
How many customers do you have in total?
*Can we sort by brand using your system?
Yes
*Can we record relationship details?  eg sales calls, appointments, emails etc...
Yes
*Can we use our ordering system Nuorder in conjunction with Zoho?
I'm not sure. Have contacted NuOrder for details about the API
*Does it talk to Mailchimp, so we can send out newsletters to specific buyers by brand?
Yes. You can also use Zoho Campaigns which does the same thing and has better integration.
"""
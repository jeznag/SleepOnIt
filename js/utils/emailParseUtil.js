import * as emailSignatureParser from './emailSignatureParser.js';

export default (function() {

    'use strict';

    var extractSignature = emailSignatureParser.extractSignature;

    /**
    * Emails often come with copies of old emails from earlier in the thread
    * We don't want to process the old emails when we're analysing as we'll have a false positive otherwise
    **/             
    function removeQuotedTextFromEmail (emailContents) {
        var processedEmail = extractSignature(emailContents).text || emailContents;

        return processedEmail;
    }

    return {
        removeQuotedTextFromEmail
    }

})();

